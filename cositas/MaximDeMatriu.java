package uf1.b0.holaM;

import java.util.Scanner;

public class MaximDeMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner sc = new Scanner(System.in);

	        int casos = sc.nextInt(); // Llegim el nombre de casos

	        for (int caso = 0; caso < casos; caso++) {
	            int filas = sc.nextInt();
	            int columnas = sc.nextInt();
	            int[][] matriz = new int[filas][columnas];
	            int max = Integer.MIN_VALUE;
	            int maxFila = 0;
	            int maxColumna = 0;

	            // Llegim els valors de la matriu i busquem el valor màxim
	            for (int i = 0; i < filas; i++) {
	                for (int j = 0; j < columnas; j++) {
	                    matriz[i][j] = sc.nextInt();
	                    if (matriz[i][j] > max) {
	                        max = matriz[i][j];
	                        maxFila = i;
	                        maxColumna = j;
	                    }
	                }
	            }

	            // Mostrem la posició del valor màxim (suma 1 a les files i columnes per a la sortida humana)
	            System.out.println((maxFila + 1) + " " + (maxColumna + 1));
	        }

	        sc.close();
	    }
	}
		
		


